# Quizz' - Backend

Le backend de Quizz' (développé avec Python et la librairie Flask) est une API qui permet d'interagir avec une base de données pour créer / gérer des quizzs, ainsi que des questions de différents types (choix unique, choix multiples, ouverte) et des réponses.

## Description de l'API
Pour documenter l'API (hors code Python), la spécification OpenAPI a été utilisée, ainsi que Swagger, l'éditeur permettant d'écrire / concevoir cette spécification.
![Prévisualisation de la descripton OpenAPI de l'API Quizz'](./assets/pictures/openapi.png)<br/>
Pour voir en détail toute la description, rendez-vous à [ce fichier](./openapi.yaml).

## Fonctionnalités implémentées (et fonctionnelles)
### Quizzs
- Récupération des quizzs et de ses questions associées
- Ajout d'un quizz
- Édition d'un quizz
- Suppression d'un quizz
- Import d'un quizz au format JSON
- Export d'un quizz au format JSON
### Questions
- Récupération d'une question et de ses réponses associées (dans la table Question directement pour une question ouverte, sinon les réponses sont stockées dans une entité dédiée)
- Ajout d'une question de type choix unique, multiples, ou ouverte
- Édition d'une question, quel que soit son type
- Suppression d'une question
### Réponses (uniquement pour des questions à choix unique / multiples)
- Récupération d'une réponse
- Ajout d'une réponse
- Édition d'une réponse
- Supression d'une réponse

## Fonctionnalités non-implémentées
- Validation des réponses aux questions d'un quizz dans le cadre d'une tentative (côté client uniquement pour le moment)

## Fonctionnalités non-fonctionnelles
- Aucune

## Pour lancer l'API
### Configuration de l'environnement
```bash
python -m venv venv/
# Ou
python3 -m venv venv/
# Ou
virtualenv venv/
```
```bash
source venv/bin/activate
```
### Installation des dépendances
```bash
pip install -r requirements.txt
```
### Commandes liées à la base de données
```bash
flask database create       # Crée une base de données avec un jeu de données inclu
flask database create-empty # Crée une base de données mais n'insère aucun contenu à l'intérieur
flask database delete       # Supprime l'ensemble des entités de la base
```
### Lancement
```bash
flask run
```
### Informations complémentaires
- L'API est accessible depuis le port 5000 sur l'hôte local
