from typing_extensions import Self

from ...app import db


class Answer(db.Model):
    """
    Représente une réponse pour une question à choix unique / multiple
    """

    __tablename__ = "Answer"

    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey("Question.id"))
    value = db.Column(db.String(100))
    correct = db.Column(db.Boolean)

    # Mappage des réponses pour les questions à choix uniques
    simple_question_ref = db.relationship(
        "SimpleQuestion",
        backref=db.backref("answers", lazy="dynamic", cascade="all, delete-orphan"),
    )

    # Mappage des réponses pour les questions à choix multiples
    multiple_question_ref = db.relationship(
        "MultipleQuestion",
        backref=db.backref("answers", lazy="dynamic", cascade="all, delete-orphan"),
    )

    def __repr__(self: Self) -> str:
        """
        Affichage par défaut de la classe
        """

        return f"<Answer n°{self.id}> {self.value}"

    def to_json(self: Self) -> dict:
        """
        Renvoie en JSON le contenu de la table
        """

        return {
            "id": self.id,
            "question_id": self.question_id,
            "value": self.value,
            "correct": self.correct,
        }
