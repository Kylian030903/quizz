from typing_extensions import Self

from ...app import db


class Quizz(db.Model):
    """
    Représente un quizz
    Un quizz comporte des questions de différents types
    """

    __tablename__ = "Quizz"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    description = db.Column(db.String(300))
    attachment = db.Column(db.String(500))
    shuffle_questions = db.Column(db.Boolean)

    def __repr__(self: Self) -> str:
        """
        Affichage par défaut de la classe
        """

        return f"<Quizz n°{self.id}> {self.name}"

    def to_json(self: Self) -> dict:
        """
        Renvoie en JSON les informations du questionnaire
        """

        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "attachment": self.attachment,
            "shuffle_questions": self.shuffle_questions,
        }

    def questions_to_json(self: Self) -> dict:
        """
        Renvoie en JSON la liste des questions correspondant au questionnaire
        """

        return [question.to_json() for question in self.questions]
