from typing_extensions import Self

from ...app import db


class Question(db.Model):
    """
    Représente une question
    Entité utilisée pour définir des questions de différents types avec différentes caractéristiques

    Cette entité ne peut pas être instanciée
    """

    __tablename__ = "Question"

    id = db.Column(db.Integer, primary_key=True)
    quizz_id = db.Column(db.Integer, db.ForeignKey("Quizz.id"))
    question_type = db.Column(db.String(50))
    title = db.Column(db.String(300))
    comment = db.Column(db.String(1000))
    attachment = db.Column(db.String(500))

    # Mappage des questions pour les quizz
    quizz_ref = db.relationship(
        "Quizz",
        backref=db.backref("questions", lazy="dynamic", cascade="all, delete-orphan"),
    )

    __mapper_args__ = {
        "polymorphic_identity": "Question",
        "polymorphic_on": "question_type",
    }


class SimpleQuestion(Question):
    """
    Représente une question à choix unique
    Plusieurs réponses peuvent être associées à la question, mais une seule seulement est correcte
    """

    __tablename__ = "SimpleQuestion"

    id = db.Column(db.Integer, db.ForeignKey("Question.id"), primary_key=True)

    __mapper_args__ = {
        "polymorphic_identity": "SimpleQuestion",
        "with_polymorphic": "*",
        "polymorphic_load": "inline",
    }

    def __init__(
        self: Self, quizz_id: int, title: str, comment: str, attachment: str
    ) -> None:
        """
        Instanciation de l'entité et de l'entité parente
        """

        super().__init__(
            quizz_id=quizz_id,
            question_type="SimpleQuestion",
            title=title,
            comment=comment,
            attachment=attachment,
        )

    def __repr__(self: Self) -> str:
        """
        Affichage par défaut de la classe
        """

        return f"<SimpleQuestion n°{self.id}> {super.title}"

    def to_json(self: Self) -> dict:
        """
        Renvoie en JSON le contenu de la table
        """

        return {
            "id": self.id,
            "quizz_id": self.quizz_id,
            "question_type": "simple",
            "title": self.title,
            "answers": [answer.to_json() for answer in self.answers],
            "comment": self.comment,
            "attachment": self.attachment,
        }


class MultipleQuestion(Question):
    """
    Représente une question à choix multiples
    Plusieurs réponses peuvent être associées à la question, et plusieurs réponses peuvent être correctes
    """

    __tablename__ = "MultipleQuestion"

    id = db.Column(db.Integer, db.ForeignKey("Question.id"), primary_key=True)

    __mapper_args__ = {
        "polymorphic_identity": "MultipleQuestion",
        "with_polymorphic": "*",
        "polymorphic_load": "inline",
    }

    def __init__(
        self: Self, quizz_id: int, title: str, comment: str, attachment: str
    ) -> None:
        """
        Instanciation de l'entité et de l'entité parente
        """

        super().__init__(
            quizz_id=quizz_id,
            question_type="MultipleQuestion",
            title=title,
            comment=comment,
            attachment=attachment,
        )

    def __repr__(self: Self) -> str:
        """
        Affichage par défaut de la classe
        """

        return f"<MultipleQuestion n°{self.id}> {super.title}"

    def to_json(self: Self) -> dict:
        """
        Renvoie en JSON le contenu de la table
        """

        return {
            "id": self.id,
            "quizz_id": self.quizz_id,
            "question_type": "multiple",
            "title": self.title,
            "answers": [answer.to_json() for answer in self.answers],
            "comment": self.comment,
            "attachment": self.attachment,
        }


class OpenQuestion(Question):
    """
    Représente une question ouverte
    La réponse est saisie librement, et comparée avec la bonne réponse renseignée
    """

    __tablename__ = "OpenQuestion"

    id = db.Column(db.Integer, db.ForeignKey("Question.id"), primary_key=True)
    answer = db.Column(db.String(100))

    __mapper_args__ = {
        "polymorphic_identity": "OpenQuestion",
        "with_polymorphic": "*",
        "polymorphic_load": "inline",
    }

    def __init__(
        self: Self,
        quizz_id: int,
        title: str,
        answer: str,
        comment: str,
        attachment: str,
    ) -> None:
        """
        Instanciation de l'entité et de l'entité parente
        """

        super().__init__(
            quizz_id=quizz_id,
            question_type="OpenQuestion",
            title=title,
            comment=comment,
            attachment=attachment,
        )

        self.answer = answer

    def __repr__(self: Self) -> str:
        """
        Affichage par défaut de la classe
        """

        return f"<OpenQuestion n°{self.id}> {super.title}"

    def to_json(self: Self) -> dict:
        """
        Renvoie en JSON le contenu de la table
        """

        return {
            "id": self.id,
            "quizz_id": self.quizz_id,
            "question_type": "open",
            "title": self.title,
            "answer": self.answer,
            "comment": self.comment,
            "attachment": self.attachment,
        }
