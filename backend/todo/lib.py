from flask import abort
from typing_extensions import Any
from validators import url, ValidationFailure

from .constants import HTTP_STATUS_CODES


def check_request_string_param(
    param: Any,
    required: bool,
    min_length: int or None,
    max_length: int or None,
    custom_error_message: str or None = None,
) -> None:
    """
    Vérifie ou non la validité / conformité d'un paramètre de type str provenant d'une requête JSON
    """

    # Vérifie si le paramètre est requis
    if required and (param is None or param == ""):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    if not required and (param is None or param == ""):
        return

    # Vérifie si le paramètre est bien de type str
    if not isinstance(param, str):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre fait bien la taille minimale renseignée
    if min_length is not None and len(param) < min_length:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre ne dépasse pas la taille maximale renseignée
    if max_length is not None and len(param) > max_length:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)


def check_request_int_param(
    param: Any, required: bool, custom_error_message: str or None = None
) -> None:
    """
    Vérifie ou non la validité / conformité d'un paramètre de type int provenant d'une requête JSON
    """

    # Vérifie si le paramètre est requis
    if required and param is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre est bien de type int
    if param is not None and not isinstance(param, int):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)


def check_request_bool_param(
    param: Any, required: bool, custom_error_message: str or None = None
) -> None:
    """
    Vérifie ou non la validité / conformité d'un paramètre de type bool provenant d'une requête JSON
    """

    # Vérifie si le paramètre est requis
    if required and param is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre est bien de type bool
    if param is not None and not isinstance(param, bool):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)


def check_request_list_param(
    param: Any, required: bool, custom_error_message: str or None = None
) -> None:
    """
    Vérifie ou non la validité / conformité d'un paramètre de type list provenant d'une requête JSON
    """

    # Vérifie si le paramètre est requis
    if required and param is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre est bien de type list
    if param is not None and not isinstance(param, list):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)


def check_request_dict_param(
    param: Any, required: bool, custom_error_message: str or None = None
) -> None:
    """
    Vérifie ou non la validité / conformité d'un paramètre de type dict provenant d'une requête JSON
    """

    # Vérifie si le paramètre est requis
    if required and param is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)

    # Vérifie si le paramètre est bien de type dict
    if param is not None and not isinstance(param, dict):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), custom_error_message)


def check_uri(value: str, custom_error_message: str or None = None) -> None:
    """
    Vérifie la validité d'une URI
    """

    validation = url(value)
    error_message = "Invalid attachment URI"

    if custom_error_message is not None:
        error_message = custom_error_message

    if isinstance(validation, ValidationFailure):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), error_message)
