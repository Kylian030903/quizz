from flask import Flask, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import BadRequest, NotFound

from .constants import HTTP_STATUS_CODES

# Instanciation de l'application Flask
app = Flask(__name__)
# Chemin vers la base de données
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///quizz.db"
# app.config["SQLALCHEMY_ECHO"] = True


# Gestion des erreurs 400 (bad request)
@app.errorhandler(HTTP_STATUS_CODES.get("BAD_REQUEST"))
def bad_request(error: BadRequest):
    error_message = "Bad request"

    # Récupération du message d'erreur s'il a été fourni en amont
    if error.description is not None:
        error_message = error.description

    return (
        jsonify(
            {
                "status_code": HTTP_STATUS_CODES.get("BAD_REQUEST"),
                "message": error_message,
            }
        ),
        HTTP_STATUS_CODES.get("BAD_REQUEST"),
    )


# Gestion des erreurs 404 (not found)
@app.errorhandler(HTTP_STATUS_CODES.get("NOT_FOUND"))
def not_found(error: NotFound):
    return (
        jsonify(
            {
                "status_code": HTTP_STATUS_CODES.get("NOT_FOUND"),
                "message": "Resource not found",
            }
        ),
        HTTP_STATUS_CODES.get("NOT_FOUND"),
    )


# Autorisation de requêtes pour les routes sur /todo/api/v1.0/ et /quizz/api/v1.0/
CORS(app, resources={r"/todo/api/v1.0/*": {"origins": "*"}})
CORS(app, resources={r"/quizz/api/v1.0/*": {"origins": "*"}})

# Instanciation de la base de données
db = SQLAlchemy(app)
