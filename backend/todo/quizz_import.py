from flask import abort
from typing_extensions import Any

from .app import db
from .constants import HTTP_STATUS_CODES, QUESTION_TYPES
from .lib import (
    check_request_bool_param,
    check_request_dict_param,
    check_request_list_param,
    check_request_string_param,
    check_uri,
)
from .models.quizz.answer import Answer
from .models.quizz.questions import MultipleQuestion, OpenQuestion, SimpleQuestion
from .models.quizz.quizz import Quizz


def parse_quizz(body: dict[str, Any]) -> Quizz or None:
    """
    Récupère depuis un objet JSON les informations nécessaires à la création d'un quizz
    Vérifie la validité des informations fournies
    Crée le quizz en base de données ensuite
    """

    name = body.get("name")
    description = body.get("description")
    attachment = body.get("attachment")
    shuffle_questions = body.get("shuffle_questions")
    questions = body.get("questions")

    # Vérification de la validité des informations
    check_request_string_param(name, True, 3, 100, "Invalid quizz name")
    check_request_string_param(description, False, 3, 300, "Invalid quizz description")
    check_request_string_param(attachment, False, 20, 500, "Invalid quizz attachment")
    check_request_bool_param(
        shuffle_questions, True, "Invalid quizz shuffle questions boolean"
    )
    check_request_list_param(questions, True, "Invalid questions body")

    if attachment is not None and attachment != "":
        check_uri(attachment)

    # Instanciation d'un quizz et ajout en base de données
    quizz: Quizz = Quizz(
        name=name,
        description=description,
        attachment=attachment,
        shuffle_questions=shuffle_questions,
    )

    db.session.add(quizz)
    db.session.commit()

    # Parsing des questions
    parse_questions(quizz.id, questions)

    # Renvoi du quizz
    return quizz


def parse_questions(quizz_id: int, questions: list) -> None:
    """
    Récupère depuis un objet JSON les informations nécessaires à la création des questions
    Vérifie la validité des informations fournies
    Crée les questions en base de données ensuite
    """

    # On ne continue pas la procédère si la liste de questions est vide
    if len(questions) == 0:
        return

    for index, question in enumerate(questions):
        # Vérification de la validité des informations
        check_request_dict_param(
            question, True, f"Invalid question body for question at index {index}"
        )

        title = question.get("title")
        question_type = question.get("question_type")
        comment = question.get("comment")
        attachment = question.get("attachment")

        # Vérification de la validité des informations
        check_request_string_param(
            title, True, 3, 100, f"Invalid question title for question at index {index}"
        )
        check_request_string_param(
            question_type,
            True,
            3,
            50,
            f"Invalid question type for question at index {index}",
        )
        check_request_string_param(
            comment,
            False,
            3,
            1000,
            f"Invalid question comment for question at index {index}",
        )
        check_request_string_param(
            attachment,
            False,
            20,
            500,
            f"Invalid question attachment for question at index {index}",
        )

        if question_type not in QUESTION_TYPES:
            abort(
                HTTP_STATUS_CODES.get("BAD_REQUEST"),
                f"Invalid question type for question at index {index}",
            )

        if attachment is not None and attachment != "":
            check_uri(
                attachment, f"Invalid attachment URI for question at index {index}"
            )

        # Parsing des questions en fonction de leur type
        if question_type == "simple" or question_type == "multiple":
            parse_simple_multiple_question(
                quizz_id,
                index,
                question_type,
                title,
                comment,
                attachment,
                question.get("answers"),
            )
        else:  # Question forcément de type ouverte
            parse_open_question(
                quizz_id, index, title, comment, attachment, question.get("answer")
            )


def parse_simple_multiple_question(
    quizz_id: int,
    question_index: int,
    question_type: str,
    title: str,
    comment: str or None,
    attachment: str or None,
    answers: list,
) -> None:
    """
    Récupère depuis un objet JSON les informations nécessaires à la création d'une question à choix unique / multiple
    Vérifie la validité des informations fournies
    Crée la question en base de données ensuite
    """

    # Instanciation de la question et ajout en base de données
    if question_type == "simple":
        question: SimpleQuestion = SimpleQuestion(
            quizz_id=quizz_id, title=title, comment=comment, attachment=attachment
        )
    else:  # Question forcément à choix multiple
        question: MultipleQuestion = MultipleQuestion(
            quizz_id=quizz_id, title=title, comment=comment, attachment=attachment
        )

    db.session.add(question)
    db.session.commit()

    # Vérification de la validité des informations
    check_request_list_param(
        answers, True, f"Invalid answers body for question at index {question_index}"
    )

    # Parsing des réponses
    parse_simple_multiple_questions_answers(
        question.id, question_index, question_type, answers
    )


def parse_simple_multiple_questions_answers(
    question_id: int, question_index: int, question_type: str, answers: dict[str, Any]
) -> None:
    """
    Récupère depuis un objet JSON les informations nécessaires à la création d'une réponse pour une question
    à choix unique / multiple

    Vérifie la validité des informations fournies
    Crée les réponses en base de données ensuite
    """

    """
    Variable pour vérifier si une réponse est déjà correcte ou non parmi la liste de réponses
    dans le cas des questions à choix unique
    """
    is_one_answer_correct = False

    for index, answer in enumerate(answers):
        # Vérification de la validité des informations
        check_request_dict_param(
            answer,
            True,
            f"Invalid answer body for question at index {question_index} and answer at index {index}",
        )

        value = answer.get("value")
        correct = answer.get("correct")

        # Vérification de la validité des informations
        check_request_string_param(
            value,
            True,
            3,
            100,
            f"Invalid answer value for question at index {question_index} and answer at index {index}",
        )
        check_request_bool_param(
            correct,
            True,
            f"Invalid correct boolean for question at index {question_index} and answer at index {index}",
        )

        if question_type == "simple" and correct and is_one_answer_correct:
            abort(
                HTTP_STATUS_CODES.get("BAD_REQUEST"),
                f"Question at index {question_index} : simple choice question cannot have more than one correct answer",
            )

        answer_entity: Answer = Answer(
            question_id=question_id,
            value=value,
            correct=correct,
        )

        db.session.add(answer_entity)
        db.session.commit()

        """
        Si la réponse est correcte, met à jour la variable pour indiquer qu'au moins une réponse de la
        question est correcte
        """
        if correct:
            is_one_answer_correct = True


def parse_open_question(
    quizz_id: int,
    question_index: int,
    title: str,
    comment: str or None,
    attachment: str or None,
    answer: Any,
) -> None:
    """
    Récupère depuis un objet JSON les informations nécessaires à la création d'une question de type ouverte
    Vérifie la validité des informations fournies
    Crée la question en base de données ensuite
    """

    # Vérification de la validité des informations
    check_request_string_param(
        answer,
        True,
        3,
        100,
        f"Invalid question answer for question at index {question_index}",
    )

    # Instanciation d'une question et ajout en base de données
    question: OpenQuestion = OpenQuestion(
        quizz_id=quizz_id,
        title=title,
        comment=comment,
        attachment=attachment,
        answer=answer,
    )

    db.session.add(question)
    db.session.commit()
