TASKS_ENDPOINT = "/todo/api/v1.0/tasks"
QUIZZ_ENDPOINT = "/quizz/api/v1.0"
HTTP_STATUS_CODES = {
    "BAD_REQUEST": 400,
    "NOT_FOUND": 404,
    "OK": 200,
    "CREATED": 201,
    "UPDATED": 204,
    "NO_CONTENT": 204,
}
QUESTION_TYPES = ["simple", "multiple", "open"]
