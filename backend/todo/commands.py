from flask.cli import AppGroup

from .app import app, db
from .models.quizz.quizz import Quizz
from .models.quizz.questions import MultipleQuestion, OpenQuestion, SimpleQuestion
from .models.quizz.answer import Answer


# Crée un groupe de commandes gérant la base de données
database_cli = AppGroup("database")


@database_cli.command("create-empty")
def create_empty() -> None:
    # Supprime le contenu de la base de données au cas où
    db.drop_all()
    db.session.commit()

    # Création des tables
    db.create_all()

    print("Base de données créée. Aucun jeu de données n'a été créé")


@database_cli.command("create")
def create() -> None:
    """
    Supprime complètement le contenu des tables existantes et insère un jeu de données pré-défini
    """

    # Supprime le contenu de la base de données au cas où
    db.drop_all()
    db.session.commit()

    # Création des tables
    db.create_all()

    # Création du questionnaire
    quizz = Quizz(
        id=1,
        name="Sword Art Online",
        description='Quizz portant sur l\'animé "Sword Art Online"',
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questionnaires/sao.webp",
        shuffle_questions=True,
    )

    # Création de la question 1
    question_1 = SimpleQuestion(
        quizz_id=1,
        title="Dans quel VRMMORPG se situe la forteresse flottante de Aincrad ?",
        comment="Aincrad se situe dans Sword Art Online. Une version améliorée existe également dans ALfheim Online. Quant à Serene Garden, il s'agit d'un jeu conçu uniquement pour les patients d'hôpitaux.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/aincrad.webp",
    )
    question_1.id = 1
    question_1_answers = [
        Answer(id=1, question_id=1, value="Sword Art Online", correct=True),
        Answer(id=2, question_id=1, value="Gun Gale Online", correct=False),
        Answer(id=3, question_id=1, value="Serene Garden", correct=False),
    ]

    # Création de la question 2
    question_2 = SimpleQuestion(
        quizz_id=1,
        title="Durant quel épisode de la saison 1 SAO est-il terminé ?",
        comment="Sword Art Online est officiellement terminé dans l'épisode \"La fin du monde\", avec la chute de la forteresse du jeu Aincrad et la destruction des données hébergées au cinquième sous-sol d'Argus, l'entreprise en charge du développement de SAO. L'épisode \"Retour au monde réel\" marque définitivement la fin de SAO, tous les joueurs survivants se retrouvant alors sans aucun danger.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/chute_aincrad.webp",
    )
    question_2.id = 2
    question_2_answers = [
        Answer(id=4, question_id=2, value="Retour au monde réel", correct=False),
        Answer(id=5, question_id=2, value="La fin du monde", correct=True),
    ]

    # Création de la question 3
    question_3 = MultipleQuestion(
        quizz_id=1,
        title="ALfheim Online est...",
        comment="ALfheim est un VRMMORPG dérivé de Sword Art Online servant initialement à réaliser des expériences sur les survivants de SAO, visant à contrôler les émotions et le cerveau humain. Le nouvel ALfheim créé pourra être aperçu en saison 2. Kirito et Asuna s'embrassent la première fois dans l'épisode 10 de la première saison, intitulé \"Intentions meutrières rouge sang\". Yuuki apparaît bien pour la première fois dans ALfheim. Enfin, c'est bien le second VRMMORPG introduit dans l'animé, le premier étant SAO.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/alfheim.webp",
    )
    question_3.id = 3
    question_3_answers = [
        Answer(
            id=6,
            question_id=3,
            value="Un VRMMORPG servant à réaliser des expériences sur les survivants de SAO",
            correct=True,
        ),
        Answer(
            id=7,
            question_id=3,
            value="Le lieu où Kirito et Asuna s'embrassent pour la première fois",
            correct=False,
        ),
        Answer(
            id=8,
            question_id=3,
            value="Le lieu où l'on aperçoit pour la première fois le personnage de Yuuki",
            correct=True,
        ),
        Answer(
            id=9,
            question_id=3,
            value="Le second VRMMORPG introduit au sein de l'animé",
            correct=True,
        ),
    ]

    # Création de la question 4
    question_4 = SimpleQuestion(
        quizz_id=1,
        title="SAO s'est terminé...",
        comment="Sword Art Online se termine le 7 novembre 2024, soit à peine deux ans après son ouverture, le 6 novembre 2022.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/retour_au_monde_reel.webp",
    )
    question_4.id = 4
    question_4_answers = [
        Answer(id=10, question_id=4, value="Le 7 novembre 2024", correct=True),
        Answer(id=11, question_id=4, value="Le 6 novembre 2024", correct=False),
    ]

    # Création de la question 5
    question_5 = OpenQuestion(
        quizz_id=1,
        title="Quelle est l'année de sortie initiale de l'animé ?",
        comment="La première saison de Sword Art Online est sortie pour la première fois le 8 juillet 2012 au Japon.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/sao_backdrop.webp",
        answer="2012",
    )
    question_5.id = 5

    # Création de la question 6
    question_6 = SimpleQuestion(
        quizz_id=1,
        title='En quelle année est lancé le VRMMORPG du nom de "Sword Art Online" ?',
        comment="Sword Art Online est lancé en 2022, le 7 novembre, après un mois de phase de bêta test (de août à septembre 2022). Concernant 2026, il s'agit de l'année où se déroule les arcs Mother's Rosario (saison 2) et Alicization (saisons 3 & 4). Enfin, 2024 est l'année de fin du jeu.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/sao_intro.webp",
    )
    question_6.id = 6
    question_6_answers = [
        Answer(id=12, question_id=6, value="2026", correct=False),
        Answer(id=13, question_id=6, value="2022", correct=True),
        Answer(id=14, question_id=6, value="2024", correct=False),
    ]

    # Création de la question 7
    question_7 = SimpleQuestion(
        quizz_id=1,
        title="Dans quel épisode Yuuki apparaît-elle pour la première fois ?",
        comment="Yuuki apparaît pour la première fois dans l'épisode \"Épée absolue\", l'épisode 19 de Sword Art Online II (arc Mother's Rosario).",
        attachment="https://cdn.kylianprevot.fr/public/Yuuki.webp",
    )
    question_7.id = 7
    question_7_answers = [
        Answer(id=15, question_id=7, value="Épée absolue", correct=True),
        Answer(id=16, question_id=7, value="Mother's Rosario", correct=False),
        Answer(id=17, question_id=7, value="Sleeping Knights", correct=False),
    ]

    # Création de la question 8
    question_8 = OpenQuestion(
        quizz_id=1,
        title="Donnez le nom de la société de production musicale de Sword Art Online",
        comment="La société de production musicale de Sword Art Online (openings, endings, OST) est Aniplex.",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/aniplex.webp",
        answer="Aniplex",
    )
    question_8.id = 8

    # Création de la question 9
    question_9 = MultipleQuestion(
        quizz_id=1,
        title="L'Augma...",
        comment="L'Augma est un casque de réalité augmentée, successeur du NergeGear ou encore de l'AmuSphere. Cependant, contrairement à ses prédécesseurs, il utilise le principe de réalité augmentée. Les fonctionnalités de cette technologie ne nécessitent donc plus une imersion totale, et il est donc tout à fait possible de l'utiliser en étant éveillé(e), en contrôlant entièrement son corps. En d'autres termes, l'Augma est une surcouche virtuelle pouvant être utilisé dans la réalité, son utilisation est donc moins dangereuse. Ce casque de réalité augmentée nous est présenté au tout début du film \"Ordinal Scale\", suite directe de Sword Art Online II, et possède la fonctionnalité FullDive comme le NerveGear. Enfin, l'Augma ne peut pas être développé par RATH, puisque cette société est introduite à la fin du film, puis dans l'arc Alicization (saison 3).",
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/augma.webp",
    )
    question_9.id = 9
    question_9_answers = [
        Answer(
            id=18,
            question_id=9,
            value="Est un casque de réalité augmentée",
            correct=True,
        ),
        Answer(
            id=19,
            question_id=9,
            value="Est une évolution du NerveGear et de l'AmuSphere",
            correct=True,
        ),
        Answer(
            id=20,
            question_id=9,
            value="Est une technologie développée par RATH",
            correct=False,
        ),
        Answer(
            id=21,
            question_id=9,
            value="Ne permet pas de profiter pleinement de son corps et de sa conditon physique",
            correct=False,
        ),
        Answer(
            id=22,
            question_id=9,
            value="Possède un mode de connexion FullDive",
            correct=True,
        ),
        Answer(
            id=23,
            question_id=9,
            value="Apparaît pour la première fois à la fin de Sword Art Online II",
            correct=False,
        ),
    ]

    # Création de la question 10
    question_10 = SimpleQuestion(
        quizz_id=1,
        title='Bien que décédée à cette période-là, Yuuki apparaît dans le film "Ordinal Scale"',
        comment='En effet, Yuuki apparaît dans le film "Ordinal Scale", à la fin, lors du combat contre le boss du 100e pallier d\'Aincrad. Elle apparaît sous forme de vision, pour encourager Asuna dans son combat avec Kitiro et ses amis.',
        attachment="https://cdn.kylianprevot.fr/public/td_archi_logicielle/questions/asuna_yuuki.webp",
    )
    question_10.id = 10
    question_10_answers = [
        Answer(id=24, question_id=10, value="Vrai", correct=True),
        Answer(id=25, question_id=10, value="Faux", correct=False),
    ]

    # Ajout des questions dans la base de données
    db.session.add(quizz)

    db.session.add(question_1)
    db.session.add(question_2)
    db.session.add(question_3)
    db.session.add(question_4)
    db.session.add(question_5)
    db.session.add(question_6)
    db.session.add(question_7)
    db.session.add(question_8)
    db.session.add(question_9)
    db.session.add(question_10)

    # Ajout des réponses aux questions à choix unique / multiple dans la base de données
    for answer in question_1_answers:
        db.session.add(answer)
    for answer in question_2_answers:
        db.session.add(answer)
    for answer in question_3_answers:
        db.session.add(answer)
    for answer in question_4_answers:
        db.session.add(answer)
    for answer in question_6_answers:
        db.session.add(answer)
    for answer in question_7_answers:
        db.session.add(answer)
    for answer in question_9_answers:
        db.session.add(answer)
    for answer in question_10_answers:
        db.session.add(answer)

    db.session.commit()

    print("Base de données créée !")


@database_cli.command("delete")
def delete() -> None:
    """
    Supprime le contenu de la base de données
    """

    db.drop_all()
    db.session.commit()

    print("Contenu de la base de données supprimé")


# Ajout des commandes personnalisées
app.cli.add_command(database_cli)
