from .app import app

import todo.routes.tasks
import todo.routes.quizz.quizz
import todo.routes.quizz.questions
import todo.routes.quizz.answers
import todo.commands
