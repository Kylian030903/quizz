from flask import abort, jsonify, make_response, request, url_for

from ..app import app
from ..models.tasks import tasks as model_tasks


ENDPOINT = "/todo/api/v1.0/tasks"


def make_public_task(task):
    new_task = dict()

    task

    for field in task:
        if field == "id":
            new_task["uri"] = url_for("get_task", task_id=task.get("id"), _external=True)

        new_task[field] = task.get(field)

    return new_task


@app.route(ENDPOINT, methods=["GET"])
def get_tasks():
    return jsonify(tasks=[make_public_task(task) for task in model_tasks])


@app.route(f"{ENDPOINT}/<int:task_id>", methods=["GET"])
def get_task(task_id: int):
    tasks = list(filter(lambda task: task.get("id") == task_id, model_tasks))

    if len(tasks) == 0:
        abort(404)

    return jsonify(make_public_task(tasks[0]))


@app.route(ENDPOINT, methods=["POST"])
def create_task():
    body = request.json

    if not body:
        abort(400)

    if not "title" in body:
        abort(400)

    title = body.get("title")
    description = body.get("description")

    if type(title) != str or len(title) == 0 or len(title) > 50:
        abort(400)

    if description is not None and (type(description) != str or len(description) == 0 or len(description) > 200):
        abort(400)

    task = {
        "id": model_tasks[-1].get("id") + 1,
        "title": title,
        "description": body.get("description", ""),
        "done": False
    }

    model_tasks.append(task)

    return (jsonify(make_public_task(task)), 201)


@app.route(f"{ENDPOINT}/<int:task_id>", methods=["PUT"])
def update_task(task_id: int):
    tasks = list(filter(lambda task: task.get("id") == task_id, model_tasks))

    if len(tasks) == 0:
        abort(404)

    body: dict[str, any] = request.json

    if not body:
        abort(400)

    title = body.get("title")
    description = body.get("description")
    done = body.get("done")

    if title is not None and (type(title) != str or len(title) < 3 or len(title) > 50):
        abort(400)

    if description is not None and (type(description) != str or len(description) < 3 or len(title) > 200):
        abort(400)

    if done is not None and type(done) != bool:
        abort(400)

    task = tasks[0]

    task["title"] = body.get("title", task.get("title"))
    task["description"] = body.get("description", task.get("description"))
    task["done"] = body.get("done", task.get("done"))

    return (jsonify(make_public_task(task)), 201)


@app.route(f"{ENDPOINT}/<int:task_id>", methods=["DELETE"])
def remove_task(task_id: int):
    tasks = list(filter(lambda task: task.get("id") == task_id, model_tasks))

    if len(tasks) == 0:
        abort(404)

    task = tasks[0]

    model_tasks.remove(task)

    return (jsonify({}), 204)
