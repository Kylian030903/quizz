from flask import abort, jsonify, request
from sqlalchemy.orm.query import SelfQuery
from typing_extensions import Any

from ...app import app, db
from ...constants import QUIZZ_ENDPOINT, HTTP_STATUS_CODES
from ...models.quizz.questions import OpenQuestion, Question
from ...models.quizz.answer import Answer
from ...lib import (
    check_request_int_param,
    check_request_string_param,
    check_request_bool_param,
)


def filter_correct_question_answers(answer: Answer) -> bool:
    """
    Renvoie vrai si la réponse à la question est correcte
    """

    return answer.correct


@app.route(f"{QUIZZ_ENDPOINT}/answer/<int:answer_id>", methods=["GET"])
def get_answer(answer_id: int):
    """
    Renvoie en JSON la réponse correspondant à l'ID renseignée
    """

    answer: Answer or None = db.session.query(Answer).filter_by(id=answer_id).first()

    if answer is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    return jsonify(answer.to_json())


@app.route(f"{QUIZZ_ENDPOINT}/answer", methods=["POST"])
def add_answer():
    """
    Ajoute une réponse à une question
    """

    body: dict[str, Any] or None = request.json

    # Traitement de la demande uniquement si les données reçues dont en JSON
    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    question_id = body.get("question_id")
    value = body.get("value")
    correct = body.get("correct")

    # Vérification de la validité des données reçues
    check_request_int_param(question_id, True, "Invalid quizz id")
    check_request_string_param(value, True, 3, 100, "Invalid answer value")
    check_request_bool_param(correct, True, "Invalid correct boolean")

    question: Question or None = (
        db.session.query(Question).filter_by(id=question_id).first()
    )

    if question is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    correct_question_answers = list(
        filter(filter_correct_question_answers, question.answers)
    )
    answer: Answer = Answer(question_id=question_id, value=value, correct=correct)

    if (
        question.question_type == "SimpleQuestion"
        and len(correct_question_answers) == 1
        and correct
    ):
        abort(
            HTTP_STATUS_CODES.get("BAD_REQUEST"),
            "A simple choice question cannot have more than one answer",
        )

    db.session.add(answer)
    db.session.commit()

    return (jsonify(answer.to_json()), HTTP_STATUS_CODES.get("CREATED"))


@app.route(f"{QUIZZ_ENDPOINT}/answer/<int:answer_id>", methods=["PUT"])
def edit_answer(answer_id: int):
    """
    Edite une réponse d'une question
    """

    answer_query: SelfQuery = db.session.query(Answer).filter_by(id=answer_id)
    answer: Answer or None = answer_query.first()

    if answer is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    # Traitement de la demande uniquement si les données reçues dont en JSON
    body: dict[str, Any] or None = request.json

    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    value = body.get("value", answer.value)
    correct = body.get("correct", answer.correct)
    question: Question or None = (
        db.session.query(Question).filter_by(id=answer.question_id).first()
    )

    # Vérification de la validité des données reçues
    check_request_string_param(value, True, 3, 100, "Invalid answer value")
    check_request_bool_param(correct, True, "Invalid correct boolean")

    if question is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    correct_question_answers = list(
        filter(filter_correct_question_answers, question.answers)
    )

    if (
        question.question_type == "SimpleQuestion"
        and len(correct_question_answers) > 1
        and correct
    ):
        abort(
            HTTP_STATUS_CODES.get("BAD_REQUEST"),
            "A simple choice question cannot have more than one answer",
        )

    answer_query.update({"value": value, "correct": correct})
    db.session.commit()

    return (
        jsonify(db.session.query(Answer).filter_by(id=answer_id).first().to_json()),
        HTTP_STATUS_CODES.get("UPDATED"),
    )


@app.route(f"{QUIZZ_ENDPOINT}/answer/<int:answer_id>", methods=["DELETE"])
def delete_answer(answer_id: int):
    """
    Supprime la réponse correspondant à l'ID renseignée
    """

    answer: Answer or None = db.session.query(Answer).filter_by(id=answer_id).first()

    if answer is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    db.session.delete(answer)
    db.session.commit()

    return (jsonify({}), HTTP_STATUS_CODES.get("NO_CONTENT"))


@app.route(f"{QUIZZ_ENDPOINT}/question/<int:question_id>/answers", methods=["DELETE"])
def delete_all_question_answers(question_id: int):
    """
    Supprime toutes les réponses liées à une question de type choix unique ou multiple
    """

    question: Question or None = (
        db.session.query(Question).filter_by(id=question_id).first()
    )

    if question is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    # Seules les questions de type choix unique ou multiple ont des réponses renseignées dans l'entité Answer
    if isinstance(question, OpenQuestion):
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    db.session.query(Answer).filter_by(question_id=question_id).delete()
    db.session.commit()

    return (jsonify({}), HTTP_STATUS_CODES.get("NO_CONTENT"))
