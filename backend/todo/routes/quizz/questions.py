from flask import abort, jsonify, request
from typing_extensions import Any

from ...app import app, db
from ...constants import QUIZZ_ENDPOINT, HTTP_STATUS_CODES, QUESTION_TYPES
from ...models.quizz.quizz import Quizz
from ...models.quizz.questions import (
    MultipleQuestion,
    OpenQuestion,
    Question,
    SimpleQuestion,
)
from ...lib import check_request_string_param, check_request_int_param, check_uri


@app.route(f"{QUIZZ_ENDPOINT}/question/<int:question_id>", methods=["GET"])
def get_question(question_id: int):
    """
    Renvoie en JSON la question correspondant à l'ID renseignée
    """

    question: Question or None = (
        db.session.query(Question).filter_by(id=question_id).first()
    )

    if question is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    return jsonify(question.to_json())


def add_simple_multiple_question(
    question_type: str, title: str, quizz_id: int, comment: str, attachment: str
):
    """
    Ajoute une question de type simple ou multiple dans la base de données
    """

    if question_type == "simple":
        question: SimpleQuestion = SimpleQuestion(
            title=title, quizz_id=quizz_id, comment=comment, attachment=attachment
        )
    else:  # Question forcément à choix multiples
        question: MultipleQuestion = MultipleQuestion(
            title=title, quizz_id=quizz_id, comment=comment, attachment=attachment
        )

    db.session.add(question)
    db.session.commit()

    return (jsonify(question.to_json()), HTTP_STATUS_CODES.get("CREATED"))


def add_open_question(
    title: str, quizz_id: int, answer: Any, comment: str, attachment: str
):
    """
    Ajoute une question de type ouverte dans la base de données
    """

    # Vérification de la validité de la réponse
    check_request_string_param(answer, True, 3, 100, "Invalid question answer")

    question: OpenQuestion = OpenQuestion(
        title=title,
        quizz_id=quizz_id,
        answer=answer,
        comment=comment,
        attachment=attachment,
    )

    db.session.add(question)
    db.session.commit()

    return (jsonify(question.to_json()), HTTP_STATUS_CODES.get("CREATED"))


@app.route(f"{QUIZZ_ENDPOINT}/question/<string:question_type>", methods=["POST"])
def add_question(question_type: str):
    """
    Ajoute une question à un questionnaire
    """

    if question_type not in QUESTION_TYPES:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), "Invalid question type")

    # Traitement de la demande uniquement si les données reçues dont en JSON
    body: dict[str, Any] or None = request.json

    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    title = body.get("title")
    quizz_id = body.get("quizz_id")
    comment = body.get("comment")
    attachment = body.get("attachment")

    # Vérification de la validité des données reçues
    check_request_string_param(title, True, 3, 300, "Invalid question title")
    check_request_int_param(quizz_id, True, "Invalid quizz id")
    check_request_string_param(comment, False, 3, 1000, "Invalid question comment")
    check_request_string_param(
        attachment, False, 20, 500, "Invalid question attachment"
    )

    if attachment is not None and attachment != "":
        check_uri(attachment)

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    # Gestion de l'ajout en fonction type des questions
    if question_type == "simple" or question_type == "multiple":
        return add_simple_multiple_question(
            question_type, title, quizz_id, comment, attachment
        )
    else:  # Vérification du type de question faite en début de fonction, donc question forcément de type ouverte
        answer = body.get("answer")

        return add_open_question(title, quizz_id, answer, comment, attachment)


def edit_simple_multiple_question(
    question_id: int, title: str, comment: str, attachment: str
):
    """
    Modifie une question de type simple ou multiple dans la base de données
    """

    db.session.query(Question).filter_by(id=question_id).update(
        {"title": title, "comment": comment, "attachment": attachment}
    )
    db.session.commit()


def edit_open_question(
    question_id: int, title: str, answer: Any, comment: str, attachment: str
):
    """
    Modifie une question de type ouverte dans la base de données
    """

    db.session.query(Question).filter_by(id=question_id).update(
        {"title": title, "comment": comment, "attachment": attachment}
    )
    db.session.query(OpenQuestion).filter_by(id=question_id).update({"answer": answer})
    db.session.commit()


@app.route(f"{QUIZZ_ENDPOINT}/question/<int:question_id>", methods=["PUT"])
def edit_question(question_id: int):
    """
    Edite une question d'un questionnaire
    """

    question: Question or None = (
        db.session.query(Question).filter_by(id=question_id).first()
    )

    if question is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    # Traitement de la demande uniquement si les données reçues dont en JSON
    body: dict[str, Any] or None = request.json

    if body is not None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"))

    title = body.get("title", question.title)
    comment = body.get("comment", question.comment)
    attachment = body.get("attachment", question.attachment)

    # Vérification de la validité des données reçues
    check_request_string_param(title, True, 3, 300, "Invalid question title")
    check_request_string_param(comment, False, 3, 1000, "Invalid question comment")
    check_request_string_param(
        attachment, False, 20, 500, "Invalid question attachment"
    )

    if attachment is not None and attachment != "":
        check_uri(attachment)

    # Gestion de l'ajout en fonction type des questions
    if (
        question.question_type == "SimpleQuestion"
        or question.question_type == "MultipleQuestion"
    ):
        edit_simple_multiple_question(question_id, title, comment, attachment)
    else:  # Question forcément de type ouverte
        answer = body.get(answer, question.answer)

        edit_open_question(question_id, title, answer, comment, attachment)

    return (
        jsonify(db.session.query(Question).filter_by(id=question_id).first().to_json()),
        HTTP_STATUS_CODES.get("UPDATED"),
    )


@app.route(f"{QUIZZ_ENDPOINT}/question/<int:question_id>", methods=["DELETE"])
def delete_question(question_id: int):
    """
    Supprime la question correspondant à l'ID renseignée
    """

    question: Question or None = (
        db.session.query(Question).filter_by(id=question_id).first()
    )

    if question is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    db.session.delete(question)
    db.session.commit()

    return (jsonify({}), HTTP_STATUS_CODES.get("NO_CONTENT"))


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>/questions", methods=["DELETE"])
def delete_all_quizz_questions(quizz_id: int):
    """
    Supprime toutes les questions d'un quizz
    """

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    db.session.query(Question).filter_by(quizz_id=quizz_id).delete()
    db.session.commit()

    return (jsonify({}), HTTP_STATUS_CODES.get("NO_CONTENT"))
