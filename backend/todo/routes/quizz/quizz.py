from uuid import uuid1
from flask import Response, abort, jsonify, make_response, request
from sqlalchemy.orm.query import SelfQuery
from typing_extensions import Any

from ...app import app, db
from ...constants import QUIZZ_ENDPOINT, HTTP_STATUS_CODES
from ...models.quizz.quizz import Quizz
from ...lib import check_request_bool_param, check_request_string_param, check_uri
from ...quizz_import import parse_quizz


@app.route(f"{QUIZZ_ENDPOINT}/quizz", methods=["GET"])
def get_all_quizzs():
    """
    Recupère l'ensemble des questionnaires de la base de données et les renvoie au format JSON
    """

    quizzs: list[Quizz] = db.session.query(Quizz).all()
    quizzs_json: list[dict[str, Any]] = []

    if len(quizzs) > 0:
        for questionnaire in quizzs:
            quizzs_json.append(questionnaire.to_json())

    return jsonify(quizzs_json)


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>", methods=["GET"])
def get_quizz(quizz_id: int):
    """
    Récupère le questionnaire correspondant à l'ID renseignée et le renvoie en JSON
    """

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    return jsonify(quizz.to_json())


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>/questions", methods=["GET"])
def get_quizz_questions(quizz_id: int):
    """
    Renvoie en JSON toutes les questions correspondant à l'ID d'un questionnaire
    """

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    return jsonify(quizz.questions_to_json())


@app.route(f"{QUIZZ_ENDPOINT}/quizz/import", methods=["POST"])
def import_quizz():
    """
    Importe un quizz issu d'un fichier JSON
    """

    body: dict[str, Any] or None = request.json

    # Traitement de la demande uniquement si les données reçues dont en JSON
    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), "Invalid JSON body")

    # Parsing de l'objet JSON récupéré pour créer le quizz, les questions et réponses en base de données
    quizz: Quizz = parse_quizz(body)

    return (jsonify(quizz.to_json()), HTTP_STATUS_CODES.get("CREATED"))


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>/export", methods=["GET"])
def export_quizz(quizz_id: int):
    """
    Exporte un quizz avec ses informations et questions en format JSON
    """

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    quizz_json = quizz.to_json()
    quizz_json["questions"] = quizz.questions_to_json()

    response: Response = make_response(jsonify(quizz_json))
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename=f"quizz{quizz.id}_{uuid1().time}.json",
    )
    response.headers.set("Content-Type", "application/json")

    return response


@app.route(f"{QUIZZ_ENDPOINT}/quizz", methods=["POST"])
def add_quizz():
    """
    Ajoute un questionnaire à la base de données
    """

    body: dict[str, Any] or None = request.json

    # Traitement de la demande uniquement si les données reçues dont en JSON
    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), "Invalid JSON body")

    name = body.get("name")
    description = body.get("description")
    attachment = body.get("attachment")
    shuffle_questions = body.get("shuffle_questions")

    # Vérification de la validité des données reçues
    check_request_string_param(name, True, 3, 100, "Invalid quizz title")
    check_request_string_param(description, False, 3, 300, "Invalid quizz description")
    check_request_string_param(attachment, False, 20, 500, "Invalid quizz attachment")
    check_request_bool_param(
        shuffle_questions, True, "Invalid quizz shuffle questions boolean"
    )

    if attachment is not None and attachment != "":
        check_uri(attachment)

    quizz: Quizz = Quizz(
        name=name,
        description=description,
        attachment=attachment,
        shuffle_questions=shuffle_questions,
    )

    db.session.add(quizz)
    db.session.commit()

    return (jsonify(quizz.to_json()), HTTP_STATUS_CODES.get("CREATED"))


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>", methods=["PUT"])
def edit_quizz(quizz_id: int):
    """
    Modifie un questionnaire de la base de données
    """

    body: dict[str, Any] or None = request.json

    # Traitement de la demande uniquement si les données reçues dont en JSON
    if body is None:
        abort(HTTP_STATUS_CODES.get("BAD_REQUEST"), "Invalid JSON body")

    quizz_query: SelfQuery = db.session.query(Quizz).filter_by(id=quizz_id)
    quizz: Quizz or None = quizz_query.first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    name = body.get("name", quizz.name)
    description = body.get("description", quizz.description)
    attachment = body.get("attachment", quizz.attachment)
    shuffle_questions = body.get("shuffle_questions", quizz.shuffle_questions)

    # Vérification de la validité des données reçues
    check_request_string_param(name, True, 3, 100, "Invalid quizz title")
    check_request_string_param(description, False, 3, 300, "Invalid quizz description")
    check_request_string_param(attachment, False, 20, 500, "Invalid quizz attachment")
    check_request_bool_param(
        shuffle_questions, True, "Invalid quizz shuffle questions boolean"
    )

    if attachment != "":
        check_uri(attachment)

    quizz_query.update(
        {
            "name": name,
            "description": description,
            "attachment": attachment,
            "shuffle_questions": shuffle_questions,
        }
    )
    db.session.commit()

    return (
        jsonify(db.session.query(Quizz).filter_by(id=quizz_id).first().to_json()),
        HTTP_STATUS_CODES.get("UPDATED"),
    )


@app.route(f"{QUIZZ_ENDPOINT}/quizz/<int:quizz_id>", methods=["DELETE"])
def delete_quizz(quizz_id: int):
    """
    Supprime le questionnaire correspondant à l'ID renseignée
    """

    quizz: Quizz or None = db.session.query(Quizz).filter_by(id=quizz_id).first()

    if quizz is None:
        abort(HTTP_STATUS_CODES.get("NOT_FOUND"))

    db.session.delete(quizz)
    db.session.commit()

    return (jsonify({}), HTTP_STATUS_CODES.get("NO_CONTENT"))
