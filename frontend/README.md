# Quizz' - Frontend

Le frontend de Quizz' (développé avec Vue.js sous l'écosystème Node.js) est une application web qui grâce au backend, permet à des utilisateurs de créer, gérer et répondre à des quizzs.

## Aperçus
<details>
<summary>Cliquez pour étendre</summary>
![Screenshot 01](./assets/pictures/Screenshot_01.png)
![Screenshot 02](./assets/pictures/Screenshot_02.png)
![Screenshot 03](./assets/pictures/Screenshot_03.png)
![Screenshot 04](./assets/pictures/Screenshot_04.png)
![Screenshot 05](./assets/pictures/Screenshot_05.png)
![Screenshot 06](./assets/pictures/Screenshot_06.png)
![Screenshot 07](./assets/pictures/Screenshot_07.png)
![Screenshot 08](./assets/pictures/Screenshot_08.png)
![Screenshot 09](./assets/pictures/Screenshot_09.png)
![Screenshot 10](./assets/pictures/Screenshot_10.png)
![Screenshot 11](./assets/pictures/Screenshot_11.png)
![Screenshot 12](./assets/pictures/Screenshot_12.png)
![Screenshot 13](./assets/pictures/Screenshot_13.png)
![Screenshot 14](./assets/pictures/Screenshot_14.png)
![Screenshot 15](./assets/pictures/Screenshot_15.png)
![Video 01](./assets/videos/Video_01.webp)
![Video 02](./assets/videos/Video_02.webp)
![Video 03](./assets/videos/Video_03.webp)
</details>

## Fonctionnalités implémentées (et fonctionnelles)
### Quizzs
- Récupération des quizzs et de ses questions associées
- Ajout d'un quizz
- Édition d'un quizz
- Suppression d'un quizz
- Import d'un quizz au format JSON
- Export d'un quizz au format JSON
- Tentative de réponse aux questions avec validation et correction des réponses
### Questions
- Récupération d'une question et de ses réponses associées (dans la table Question directement pour une question ouverte, sinon les réponses sont stockées dans une entité dédiée)
- Ajout d'une question de type choix unique, multiples, ou ouverte
- Édition d'une question, quel que soit son type
- Suppression d'une question
### Réponses (uniquement pour des questions à choix unique / multiples)
- Récupération d'une réponse
- Ajout d'une réponse
- Édition d'une réponse
- Supression d'une réponse

## Fonctionnalités non-implémentées
- Aucune

## Fonctionnalités non-fonctionnelles
- Aucune

## Pour lancer l'application web
### Installation des dépendances
```bash
npm install
# Ou
yarn
```
### Lancement
```bash
npm run dev
# Ou
yarn dev
```
### Informations complémentaires
- L'application web est accessible depuis le port 5173 de l'hôte local. Toutefois, un autre numéro de port sera utilisé si celui-ci est déjà utilisé sur l'hôte
