type QuestionType = "open" | "simple" | "multiple";

interface QuestionRaw {
  id: number;
  quizz_id: number;
  title: string;
  comment?: string;
  attachment?: string;
}

interface Answer {
  id: number;
  question_id: number;
  value: string;
  correct: boolean;
}

interface SimpleMultipleQuestion extends QuestionRaw {
  answers: Answer[];
}

interface SimpleQuestion extends SimpleMultipleQuestion {
  question_type: "simple";
}

interface MultipleQuestion extends SimpleMultipleQuestion {
  question_type: "multiple";
}

interface OpenQuestion extends QuestionRaw {
  question_type: "open";
  answer: string;
}

type Question = SimpleQuestion | MultipleQuestion | OpenQuestion;

export type { QuestionType, SimpleQuestion, MultipleQuestion, OpenQuestion, Question, Answer };
