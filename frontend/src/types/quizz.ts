interface Quizz {
  id: number;
  name: string;
  description?: string;
  attachment?: string;
  shuffle_questions: boolean;
}

export type { Quizz };
