import { createApp } from "vue";
import router from "./router";
import root from "./app.vue";
import icons from "v-svg-icons";
import "@/assets/css/main.css";
import "@/assets/css/fallback.css";

const app = createApp(root);

app.use(router);
app.component("v-icon", icons);
app.mount("#app");
