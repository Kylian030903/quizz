import {
  createRouter,
  createWebHistory,
  type NavigationGuardNext,
  type RouteLocationNormalized,
  type Router,
  type RouteRecordRaw,
} from "vue-router";
import notFoundComponentVue from "@/components/not-found-component.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/home",
    name: "Home",
    component: () => import("@/pages/home-page.vue"),
    meta: {
      title: "Quizz'",
    },
  },
  {
    path: "/",
    name: "HomeRedirect",
    redirect: "/home",
  },
  {
    path: "/quizz/add",
    name: "AddQuizz",
    component: () => import("@/pages/quizz/add-quizz.vue"),
    meta: {
      title: "Quizz' - Ajout de quizz",
    },
  },
  {
    path: "/quizz/:id/edit",
    name: "EditQuizz",
    component: () => import("@/pages/quizz/edit-quizz.vue"),
    meta: {
      title: "Quizz' - Éditeur",
    },
  },
  {
    path: "/quizz/:id/play",
    name: "PlayQuizz",
    component: () => import("@/pages/quizz/play-quizz.vue"),
    meta: {
      title: "Quizz' - Tentative",
    },
  },
  {
    path: "/question/add",
    name: "AddQuestion",
    component: () => import("@/pages/add-question.vue"),
    meta: {
      title: "Quizz' - Ajout de question",
    },
  },
  {
    path: "/answer/add",
    name: "AddAnswer",
    component: () => import("@/pages/add-answer.vue"),
    meta: {
      title: "Quizz' - Ajout de réponse",
    },
  },
  {
    path: "/:patchMatch(.*)*",
    name: "NotFound",
    component: notFoundComponentVue,
    meta: {
      title: "Quizz' - 404",
    },
  },
];
const router: Router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach(
  (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
    // Get the page title from the route meta data that we have defined
    // See further down below for how we setup this data
    const title = to.meta.title as string;

    // If the route has a title, set it as the page title of the document/page
    if (title) {
      document.title = title;
    }

    // Continue resolving the route
    next();
  },
);

export default router;
