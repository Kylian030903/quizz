import type { QuestionType } from "./types/questions";

interface FetchEndpointProps {
  confirmationMessage?: string;
  endpoint: string;
  method?: "GET" | "POST" | "PUT" | "DELETE";
  body?: string;
  successMessage?: string;
  errorMessage?: string;
  onSucess?: () => void | Promise<void>;
  onError?: (statusCode?: number) => void | Promise<void>;
}

/**
 * Fonction factorisant les requêtes fetch et ajoutant des fonctionnalités supplémentaires
 * (demande de confirmation, message de succès, d'erreur, gestion des erreurs)
 */
const fetchEndpoint = async <APIResponse = unknown>({
  confirmationMessage,
  endpoint,
  method = "GET",
  body,
  successMessage,
  errorMessage,
  onSucess,
  onError,
}: FetchEndpointProps): Promise<APIResponse | null> => {
  /**
   * Si un message de confirmation est défini, l'utilisateur sera averti avant que la
   * requête s'exécute
   **/
  if (confirmationMessage && !confirm(confirmationMessage)) {
    return null;
  }

  try {
    let options: RequestInit = {
      method,
    };

    /**
     * Ajoute le header Content-Type aux options uniquement pour une requête POST ou PUT
     * Ajoute le body aux options uniquement pour une requête POST ou PUT
     **/
    if (method === "POST" || method === "PUT") {
      options = {
        ...options,
        headers: {
          "Content-Type": "application/json",
        },
        body,
      };
    }

    const response: Response = await fetch(endpoint, options);

    // Gestion des erreurs
    if (!response.ok || response.status === 400 || response.status === 404) {
      if (onError) {
        onError(response.status);
      }

      if (errorMessage) {
        alert(errorMessage);
      }

      return null;
    }

    if (onSucess) {
      onSucess();
    }

    if (successMessage) {
      alert(successMessage);
    }

    // Dans le cas d'un DELETE, aucune donnée n'est renvoyée
    return method !== "DELETE" ? await response.json() : null;
  } catch (error) {
    if (onError) {
      onError();
    }

    if (errorMessage) {
      alert(errorMessage);
    }

    return null;
  }
};

const formatQuestionType = (questionType: QuestionType): string => {
  switch (questionType) {
    case "open":
      return "réponse ouverte";

    case "simple":
      return "choix unique";

    case "multiple":
      return "choix multiple";

    default:
      return "type inconnu";
  }
};

const shuffleArray = (array: unknown[]): void => {
  for (let i = array.length - 1; i > 0; i--) {
    const j: number = Math.floor(Math.random() * (i + 1));
    const temp: unknown = array[i];

    array[i] = array[j];
    array[j] = temp;
  }
};

export { fetchEndpoint, formatQuestionType, shuffleArray };
