/**
 * Définit toutes les routes de l'API à utiliser pour effectuer les requêtes de récupération,
 * d'ajout, d'édition et de suppression
 *
 * Permet de former une URI en fonction de la route demandée et des paramètres fournis
 */
class Endpoints {
  // URL d'origine de toutes les routes
  private static ORIGIN = "http://localhost:5000/quizz/api/v1.0";

  // Routes pour les questionnaires
  public static QUIZZ_ROOT = `${Endpoints.ORIGIN}/quizz`; // GET (tous les questionnaires), POST
  public static QUIZZ = `${Endpoints.ORIGIN}/quizz/:id`; // GET, PUT et DELETE
  public static QUIZZ_QUESTIONS = `${Endpoints.ORIGIN}/quizz/:id/questions`; // GET et DELETE (toutes les questions d'un quizz)
  public static QUIZZ_IMPORT = `${Endpoints.ORIGIN}/quizz/import`; // POST
  public static QUIZZ_EXPORT = `${Endpoints.ORIGIN}/quizz/:id/export`; // GET

  // Routes pour les questions
  public static QUESTION_ROOT = `${Endpoints.ORIGIN}/question/:type`; // POST
  public static QUESTION = `${Endpoints.ORIGIN}/question/:id`; // GET, PUT et DELETE
  public static QUESTION_ANSWERS = `${Endpoints.ORIGIN}/question/:id/answers`; // DELETE (toutes les réponses d'une question)

  // Routes pour les réponses
  public static ANSWER_ROOT = `${Endpoints.ORIGIN}/answer`; // POST
  public static ANSWER = `${Endpoints.ORIGIN}/answer/:id`; // GET, PUT et DELETE

  /**
   * Permet de former une URI en fonction de la route demandée et des paramètres fournis
   */
  public static getEndpoint(endpoint: string, param?: string): string {
    // Insertion des valeurs (facultatif, dépend de la route utilisée)
    if (param) {
      return endpoint.replace(":id", param).replace(":type", param);
    }

    return endpoint;
  }
}

export default Endpoints;
